<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');

Route::get('/playlists/{page}', 'PlaylistApiController@index');
Route::get('/playlist/random', 'PlaylistApiController@getRandomPlaylist');
Route::get('/playlist/track/random', 'PlaylistApiController@getRandomTrack');
Route::get('/playlist/{playlistUrl}', 'PlaylistApiController@show');


// Catch all for all other api routes
Route::get('/{param1?}/{param2?}/{param3?}', 'PlaylistApiController@error');
