<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('prettyJson', function ($data, $status = 200, $headers = []) {
            return Response::json($data, $status, $headers, JSON_PRETTY_PRINT);
        });
    }
}