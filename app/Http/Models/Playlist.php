<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'date',
        'spotifyId',
        'image',
        'webLink',
        'trackCount'
    ];

    /**
     * Added attribute
     *
     * @var array
     */
    protected $appends = [
        'url'
    ];

    /**
     * The hasMany relation.
     * 
     */
    public function tracks()
    {
        return $this->hasMany('App\Models\Track');
    }

    /**
     * Scope a query to order by date descending.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('date', 'desc')->get();
    }

    /**
     * Scope a query to take a page of items.
     *
     * @param  int $limit
     * @param  int $page
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePage($query, $limit, $page)
    {
        $skip = $limit * $page;
        return $query->orderBy('date', 'desc')->skip($skip)->take($limit)->get();
    }

    /**
     * Edit URL mutator.
     * 
     * @return string
     */
    public function getUrlAttribute()
    {
        return strtolower(preg_replace("/[^A-Za-z0-9]/", '-', $this->name));
    }

    /**
     * Edit Link mutator.
     * 
     * @return string
     */
    public function getAdminEditLinkAttribute()
    {
        return route('admin.playlist.edit', ['playlistId' => $this->id]);
    }

    /**
     * Delete Link mutator.
     * 
     * @return string
     */
    public function getAdminDeleteLinkAttribute()
    {
        return route('admin.playlist.delete', ['playlistId' => $this->id]);
    }

    /**
     * Create Link mutator.
     * 
     * @return string
     */
    public function getAdminCreateLinkAttribute()
    {
        if ($this->id) return null;
        return route('admin.playlist.createFromId', ['spotifyId' => $this->spotifyId, 'name' => urlencode(str_replace("/", " ", $this->name))]);
    }

    /**
     * Populate model with data pulled from Spotify WebAPI.
     * 
     * @param  array|object $spotifyPlaylist
     * @param  bool $includeLocal (optional)
     * @param  bool $refreshData (optional)
     * @return bool
     */
    public function populatePlaylist($spotifyPlaylist, $includeLocal = true, $refreshData = false)
    {
        if (!$spotifyPlaylist) return false;
        $this->_setPlaylistData($spotifyPlaylist, $refreshData);

        // Add tracks from api
        $this->tracks = $this->_setPlaylistTracks($spotifyPlaylist, $includeLocal);
        return true;
    }

    /**
     * Populate/update data, and optionally save.
     * 
     * @param  array|object $spotifyPlaylist
     * @param  bool $refreshData (optional)
     * @return bool
     */
    private function _setPlaylistData($spotifyPlaylist, $refreshData)
    {
        if (isset($spotifyPlaylist->images[2])){
            $this->image = $spotifyPlaylist->images[2]->url;
        }
        if (isset($spotifyPlaylist->tracks)){
            $this->trackCount = $spotifyPlaylist->tracks->total;
        }
        if (isset($spotifyPlaylist->external_urls->spotify)){
            $this->webLink = $spotifyPlaylist->external_urls->spotify;
        }
        

        if ($refreshData) $this->save();
    }

    /**
     * Populate tracks, and optionally include local tracks.
     * 
     * @param  array|object $spotifyPlaylist
     * @param  bool $includeLocal (optional)
     * @return bool
     */
    private function _setPlaylistTracks($spotifyPlaylist, $includeLocal = true)
    {
        $playlistTracks = [];
        if (is_array($spotifyPlaylist->tracks->items))
        {
            foreach ($spotifyPlaylist->tracks->items as $key => $trackItem)
            {
                if (!$includeLocal && $trackItem->is_local) continue;
                $newTrack = new Track();
                $newTrack->key = 'track-'.$key;
                $newTrack->spotifyId = $trackItem->track->id;
                $newTrack->name = $trackItem->track->name;
                $newTrack->isLocal = $trackItem->is_local == 1;
                $newTrack->playlist_id = $this->id;
                $newTrack->album = $trackItem->track->album->name;

                if (isset($trackItem->track->artists[0])){
                    $newTrack->artist = $trackItem->track->artists[0]->name;
                }

                if (isset($trackItem->track->album->images[2])){
                    $newTrack->image = $trackItem->track->album->images[2]->url;
                }
                
                $playlistTracks[] = $newTrack;
            }
        }
        return $playlistTracks;
    }
}
