<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Playlist;

class Track extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'spotifyId',
        'name',
        'artist',
        'album',
        'image',
        'isLocal',
        'playlist_id'
    ];

    /**
     * Added attribute
     *
     * @var array
     */
    protected $appends = [
        'playlist'
    ];

    /**
     * The belongsTo relation.
     * 
     */
    public function playlist()
    {
        return $this->belongsTo('App\Models\Playlist', 'playlist_id');
    }

    /**
     * Local track display mutator.
     * 
     * @return string
     */
    public function getIsLocalDisplayAttribute()
    {
        return $this->isLocal ? 'Yes' : 'No';
    }

    /**
     * Playlist mutator.
     * 
     * @return App\Models\Playlist
     */
    public function getPlaylistAttribute()
    {
        if (!$this->playlist_id) return null;
        return Playlist::find($this->playlist_id); 
    }
    
}
