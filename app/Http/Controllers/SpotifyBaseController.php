<?php namespace App\Http\Controllers;
// use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Request;
// use Illuminate\Support\Facades\Input;

use App\Models\Playlist;
use SpotifyWebAPI;

class SpotifyBaseController extends Controller
{
    // TODO: get from config
    private $CLIENT_ID = '0e719e012b6e422fbd1829ac003cad90';
    private $CLIENT_SECRET = 'ada8a8239d374d938bd1b7f4c9b4bdaa';
    private $PLAYLIST_USER = 'petegaulton';

    private $REDIRECT_URI;
    /**
     * Create a new SpotifyBaseController instance, setting REDIRECT_URI
     *
     * @return void
     */    
    public function __construct()
    {
        $this->REDIRECT_URI = route('admin.spotify.authResponse');
    }

    /**
     * Check if we have Spotify 'Authorization Code flow' Auth.
     *
     * @return bool
     */
    protected function _getAuthStatus()
    {
        $api = $this->_getApi();

        return $api ? true : false;
    }

    /**
     * Get Spotify WebAPI with 'Authorization Code flow'.
     *
     * @return SpotifyWebAPI\SpotifyWebAPI
     */
    private function _getApi()
    {
        // Get access token from session
        $accessToken = Request::session()->get('spotifyAccessToken');
        if (!$accessToken) return null;

        // Set the access token on the API wrapper
        $api = new SpotifyWebAPI\SpotifyWebAPI();
        $api->setAccessToken($accessToken);

        // Test api
        $testResponse = $this->_testApi($api);   
        if ($testResponse) return $api;

        // Get refresh
        $refreshToken = isset($_SESSION['spotifyRefreshToken']) ? $_SESSION['spotifyRefreshToken'] : null;
        if (!$refreshToken) return null;

        // Do refresh & re-test
        $session = $this->_getSpotifySessionObject();
        $session->refreshAccessToken($refreshToken);
        $testResponse = $this->_testApi($api);   

        return $testResponse ? $api : null;
    }

    /**
     * Check Spotify WebAPI object is authed.
     *
     * @param SpotifyWebAPI\SpotifyWebAPI
     * @return bool
     */
    protected function _testApi(SpotifyWebAPI\SpotifyWebAPI $api)
    {
        try {
            if (!$api) return false;

            $user = $api->getUser($this->PLAYLIST_USER);
            return true;
        }
        catch (\Exception $ex) {
            //echo 'Caught exception: ',  $ex->getMessage(), "\n";
        }

        return false;
    }

    


    /**
     * Get Spotify WebAPI with 'Client Credentials flow'.
     *
     * @return SpotifyWebAPI\SpotifyWebAPI
     */
    private function _getClientApi()
    {
        $session = $this->_getSpotifySessionObject();
        $scopes = array(
            'playlist-read-private',
            'user-read-private'
        );
        $session->requestCredentialsToken($scopes);
        $accessToken = $session->getAccessToken();
        $api = new SpotifyWebAPI\SpotifyWebAPI();
        $api->setAccessToken($accessToken);
        return $api;
    }

    /**
     * Perform Authentication redirect for Spotify WebAPI with 'Authorization Code flow'.
     *
     * @return \Illuminate\Http\Response
     */
    protected function _doAuthRedirect()
    {
        $session = $this->_getSpotifySessionObject();

        $scopes = array(
            'playlist-read-private',
            'user-read-private'
        );

        $authorizeUrl = $session->getAuthorizeUrl(array(
            'scope' => $scopes
        ));
        return redirect($authorizeUrl);
    }

    /**
     * Get new Spotify Session object.
     *
     * @return SpotifyWebAPI\Session
     */
    protected function _getSpotifySessionObject()
    {
        return new SpotifyWebAPI\Session(
            $this->CLIENT_ID, 
            $this->CLIENT_SECRET, 
            $this->REDIRECT_URI
        );
    }



    /**
     * Get a page of authed user's playlists.
     *
     * @param  int $page
     * @param  int $limit
     * @return array|object The user's playlists.
     */
    public function _getPlaylists($page, $limit)
    {
        $api = $this->_getApi();
        if (!$api) return null;

        $offset = $limit * $page;
        $playlists = $api->getMyPlaylists(['limit' => $limit, 'offset' => $offset]);

        return $playlists;
    }

    /**
     * Get specific playlist for authed user.
     *
     * @return array|object The user's playlists.
     */
    protected function _getPlaylist($spotifyPlaylistId)
    {
        try {
            $api = $this->_getClientApi();
            if (!$api) return null;

            $spotifyPlaylist = $api->getUserPlaylist($this->PLAYLIST_USER, $spotifyPlaylistId);
            return $spotifyPlaylist;
        }
        catch (\Exception $ex) {
            // echo 'Caught exception: ',  $ex->getMessage(), "\n";
            // die();
        }

        return null;
    }


    // TODO: remove or improve
    protected function _debug($item, $die = true)
    {
        print '<pre>';
        print_r($item);
        print '</pre>';
        if ($die) die();
    }
    
}
