<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;
use App\Models\Playlist;

class SiteController extends SpotifyBaseController
{
    /**
     * Return basic view, let react do the rest.
     *
     * @return \Illuminate\Http\Response
     */    
    public function index()
    {
//        $requestPath = Request::path();
        $markup = Request::input('markup');

        return view('site/index', compact('markup'));
    }

}
