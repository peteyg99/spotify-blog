<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request; 

use App\Models\Playlist;
use Carbon\Carbon;

class PlaylistApiController extends SpotifyBaseController
{
    /**
     * Create a new PlaylistApiController instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a list of playlists.
     *
     * @param int $page
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($page = 0, $pageSize = 10)
    {
        $playlists = Playlist::page($pageSize, $page);
        $totalPlaylists = Playlist::count();
        $lastPage = floor(($totalPlaylists - 1) / $pageSize);

        return Response::prettyJson([
            'playlists' => $playlists,
            'lastPage' => $lastPage
        ]);
    }

    /**
     * Get a playlist.
     *
     * @param string $playlistUrl
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($playlistUrl)
    {
        $playlist = $this->_findPlaylistByUrl($playlistUrl);
        if (!$playlist){
            return Response::json([
                'error' => [
                    'message' => 'Not found!',
                    'status_code' => 404
                ]
            ], 404);
        }

        $spotifyPlaylist = $this->_getPlaylist($playlist->spotifyId);
        $playlist->populatePlaylist($spotifyPlaylist);

        return Response::prettyJson([
            'playlist' => $playlist
        ]);
    }

    /**
     * Get a random playlist.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomPlaylist()
    {
        $playlist = $this->_getRandomPlaylist();

        return Response::prettyJson([
            'playlist' => $playlist
        ]);
    }

    /**
     * Get a random playlist track.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomTrack()
    {
        $playlist = $this->_getRandomPlaylist();

        $randTrackKey = rand(0, count($playlist->tracks) - 1);
        $track = $playlist->tracks[$randTrackKey];

        return Response::prettyJson([
            'track' => $track
        ]);
    }

    /**
     * Catch all for other API routes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function error()
    {
        return Response::json([
            'error' => [
                'message' => 'Not found!',
                'status_code' => 404
            ]
        ], 404);
    }

    /**
     * Get random playlist.
     *
     * @return null|App\Models\Playlist
     */
    private function _getRandomPlaylist()
    {
        $playlists = Playlist::all();
        if (!$playlists || count($playlists) < 1) return null;
        
        $randPlaylistKey = rand(0, count($playlists) - 1);
        $playlist = $playlists[$randPlaylistKey];

        $spotifyPlaylist = $this->_getPlaylist($playlist->spotifyId);
        $playlist->populatePlaylist($spotifyPlaylist, false);

        return $playlist;
    }

    /**
     * Find playlist by url.
     *
     * @param  string $playlistUrl
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function _findPlaylistByUrl($playlistUrl)
    {
        $playlists = Playlist::all();
        $filtered = $playlists->filter(function($model) use ($playlistUrl){
            return $model->url == $playlistUrl;
        });
        
        return $filtered->first();

    }
}
