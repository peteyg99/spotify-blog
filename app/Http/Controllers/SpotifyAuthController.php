<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpotifyAuthController extends SpotifyBaseController
{
    /**
     * Create a new AdminSpotifyController instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Attempt Spotify 'Client Credentials flow' auth.
     *
     * @return \Illuminate\Http\Response
     */
    public function auth()
    {
        return $this->_doAuthRedirect();
    }

    /**
     * Handle Spotify WebAPI auth response.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function authResponse(Request $request)
    {
        $session = $this->_getSpotifySessionObject();

        // Request a access token using the code from Spotify
        $code = $request->input('code');
        $session->requestAccessToken($code);
        $accessToken = $session->getAccessToken();
        $refreshToken = $session->getRefreshToken();

        // Store access token in session
        // TODO: make into 1 object, implement expiration
        $request->session()->put('spotifyAccessToken', $accessToken);
        $request->session()->put('spotifyRefreshToken', $refreshToken);

        return redirect('admin');
    }
}
