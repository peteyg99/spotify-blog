<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request; 

use App\Models\Playlist;
use Carbon\Carbon;

class PlaylistController extends SpotifyBaseController
{
    /**
     * Create a new PlaylistController instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Playlist create form.
     *
     * @param string $spotifyId
     * @param string $name
     * @return \Illuminate\Http\Response
     */
    public function create($spotifyId = null, $name = null)
    {
        $playlist = new Playlist();
        $playlist->spotifyId = $spotifyId;
        $playlist->name = urldecode($name);

        $playlist->date = $this->_getDefaultDate($playlist);
        
        return $this->_getPlaylistEditForm($playlist, 'Create playlist');
    }

    /**
     * Playlist edit form.
     *
     * @param int $playlistId
     * @return \Illuminate\Http\Response
     */
    public function edit($playlistId)
    {
        $playlist = Playlist::find($playlistId);

        $spotifyPlaylist = $this->_getSpotifyPlaylist($playlist);
        if (isset($spotifyPlaylist->name)) $spotifyPlaylistName = $spotifyPlaylist->name;
        $playlist->populatePlaylist($spotifyPlaylist, true, true);

        return $this->_getPlaylistEditForm($playlist, 'Edit playlist', $spotifyPlaylistName);
    }

    /**
     * Playlist save/update callback.
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $playlistId = $request->get('id');
        if (!$playlistId)
        {
            $playlist = Playlist::create($request->all());
        }
        else 
        {
            $playlist = Playlist::find($playlistId);
            $playlist->update($request->all());
        }
        return redirect(route('admin.playlist.edit', ['playlistId' => $playlist->id]));
    }

    /**
     * Playlist delete callback.
     *
     * @param int $playlistId
     * @return \Illuminate\Http\Response
     */
    public function delete($playlistId)
    {
        $playlist = Playlist::find($playlistId);
        $playlist->delete();
        return redirect('admin');
    }

    /**
     * Attempt to create a date form Playlist name
     *
     * @param App\Models\Playlist $playlist
     * @return Carbon\Carbon
     */
    private function _getDefaultDate($playlist)
    {
        $defaultDate = null;
        try {
            $defaultDate = new Carbon('first day of '.$playlist->name);
        } catch(Exception $ex){
            $test = $ex->getMessage();
        } finally {
            if (!$defaultDate) $defaultDate = new Carbon('first day of this month');
            return $defaultDate;
        }
    }

    /**
     * Return Playlist create/edit form.
     *
     * @param App\Models\Playlist $playlist
     * @param string $formTitle
     * @param string $spotifyPlaylistName
     * @return \Illuminate\Http\Response
     */
    private function _getPlaylistEditForm($playlist, $formTitle, $spotifyPlaylistName = null)
    {
        $classes = (object) [
            'label' => 'form-label',
            'text' => 'form-control',
            'submit' => 'btn btn-primary'
        ];

        return view('admin/playlist/edit', compact('playlist', 'classes', 'formTitle', 'spotifyPlaylistName'));
    }

    /**
     * Get playlist from Spotify WebAPI.
     *
     * @param App\Models\Playlist $playlist
     * @return array|object
     */
    private function _getSpotifyPlaylist(Playlist $playlist)
    {
        if (!$playlist->spotifyId) return null;
        return $this->_getPlaylist($playlist->spotifyId);
    }
}
