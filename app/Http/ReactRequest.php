<?php namespace App\Http;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;

class ReactRequest extends Request {

    /**
	 * Create a new Illuminate HTTP request from server variables.
	 *
	 * @return static
	 */
	public static function create($uri, $params)
	{
		static::enableHttpMethodParameterOverride();

        $uri = 'http://spotify.gaulton.me/' . $uri;

		return static::createFromBase(SymfonyRequest::create($uri, 'GET', $params));
	}

}