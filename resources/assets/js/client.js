import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'


(function() {
    "use strict"
    const appRoot = document.getElementById("root")
    ReactDOM.render(React.createElement(App), appRoot)
})()