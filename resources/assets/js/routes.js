import React from 'react'
import { Route, IndexRoute } from 'react-router'
import MainLayout from './components/layout/MainLayout'
import Home from './containers/Home'
import Playlist from './containers/Playlist'
import NotFoundPage from './components/NotFoundPage'


const routes = (
  <Route path="/" component={MainLayout}>
    <IndexRoute component={Home} />
    <Route path="/page/:pageNum" component={Home} />
    <Route path="/playlist/:playlistUrl" component={Playlist} />
    <Route path="*" component={NotFoundPage}/>
  </Route>
)
export default routes
