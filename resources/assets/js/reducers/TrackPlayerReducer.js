export default function trackPlayerReducer(state = {}, action) {
    switch (action.type) {
        case 'TRACK_UPDATE':
            return {
                track: action.track,
                playlist: null,
                isLoaded: true
            }
        case 'PLAYLIST_UPDATE':
            return {
                track: null,
                playlist: action.playlist,
                isLoaded: true
            }
        case 'PLAYER_CLOSE':
            return {
                track: null,
                playlist: null,
                isLoaded: false
            }
        default:
            return state
    }
}
