﻿export default function loadedPlaylistReducer(state = {}, action) {
    switch (action.type) {
        case 'PLAYLIST_GET':
            return {
                playlist: null, // clean 
                isLoading: true,
                showExtraButtons: false
            }
        case 'PLAYLIST_RESPONSE':
            return {
                ...state,
                playlist: action.playlist,
                isLoading: false
            }
        default:
            return state
    }
}
