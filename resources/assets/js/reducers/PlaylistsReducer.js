﻿export default function playlistsReducer(state = {}, action) {
    switch (action.type) {
        case 'PLAYLISTS_GET':
            return {
                pageNum: action.pageNum,
                lastPage: state.lastPage,
                playlists: {
                    ...state.playlists,
                    //list: null, // clean list
                    isLoading: true
                }
            }
        case 'PLAYLISTS_RESPONSE':
            return {
                pageNum: action.pageNum,
                lastPage: action.lastPage,
                playlists: {
                    list: action.playlists,
                    isLoading: false
                }
            }

        default:
            return state
    }
}
