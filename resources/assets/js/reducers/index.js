﻿import { combineReducers } from 'redux'

// Reducers
import loadedPlaylistReducer from './LoadedPlaylistReducer'
import playlistsReducer from './PlaylistsReducer'
import trackPlayerReducer from './TrackPlayerReducer'

// Combine Reducers
export default combineReducers({
    playlistList: playlistsReducer,
    loadedPlaylist: loadedPlaylistReducer,
    trackPlayer: trackPlayerReducer
})
