import React from 'react'
import { Link } from 'react-router'

export default class Playlist extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showExtraButtons: false
        }
    }

    componentWillMount() {
        this.props.willMount()
    }

    componentDidUpdate() {
        this.props.didUpdate()
    }

    _sendTrackToPlayer(track) {
        this.props.sendTrackToPlayer(track)
    }

    _sendPlaylistToPlayer() {
        this.props.sendPlaylistToPlayer()
    }

    _getTrackClass(track) {
        if (track.spotifyId == this.props.currentTrackId) return "active"
        return "track-clickable"
    }

    _getTracklist(){
        const playlist = this.props.playlist
        if (!playlist || playlist.tracks == null) return null

        return playlist.tracks.map((track) => {
            if (track.isLocal)
                return (
                    <li key={track.key}>
                        <span className="track-image">
                            <span className="fa fa-warning"></span>
                        </span>
                        <span className="track-name">{track.name}</span>
                        <span className="track-artist">{this._getTrackSubHeading(track)} - Not on spotify, but look it up!</span>                        
                    </li>                    
                )

            return (
                <li key={track.key} onClick={this._sendTrackToPlayer.bind(this, track)} className={this._getTrackClass(track)}>
                    <span className="track-image">
                        <img src={track.image} />
                        <span className="fa fa-chevron-circle-down fa-hide"></span>
                    </span>
                    <span className="track-name">{track.name}</span>
                    <span className="track-artist">{this._getTrackSubHeading(track)}</span>                        
                </li>
            )
        });
    }

    _getTrackSubHeading(track){
        let subHeadingItems = []
        if (track.artist) subHeadingItems.push(track.artist) 
        if (track.album) subHeadingItems.push(track.album) 
        return subHeadingItems.join(" - ")
    }

    _getPlaylistNav(){
        let moreButtonClass = "btn btn-alt"
        let extraButtonsWrapClass = "extra-buttons-wrap"
        if (this.state.showExtraButtons) {
            moreButtonClass += " remove"
            extraButtonsWrapClass += " show"
        }

        return (
            <aside className="track-list-nav">
                <button className="btn" type="button" onClick={this._sendPlaylistToPlayer.bind(this)}>
                    Play all
                </button>

                <button type="button" className={moreButtonClass} onClick={this._showExtraButtons.bind(this)}>
                    <span className="fa fa-chevron-down"></span>
                </button>

                <div className={extraButtonsWrapClass}>
                    <a className="btn" href={this.props.playlist.webLink} target="_blank">
                        Web link
                    </a>
                </div>
            </aside>
        )
    }

    _showExtraButtons(){
        this.setState({
            ...this.state,
            showExtraButtons: true
        })
    }

    render(){
        if (this.props.isLoading)
            return (
                <div className="alert alert-info"><em>Loading playlist...</em></div>
            )

        const playlist = this.props.playlist

        if (!playlist)
            return (
                <div className="alert alert-danger">No playlist</div>
            )

        return (
            <div className="flex-wrap">
                {this._getPlaylistNav()}
                <main>
                    <h1>{this.props.playlist.name}</h1>
                    <ul className="track-list">
                        {this._getTracklist()}
                    </ul>
                </main>

            </div>
        )
    }    
}
