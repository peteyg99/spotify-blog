import React from 'react'
import { Link } from 'react-router'

export default class TrackPlayer extends React.Component {
    constructor(props) {
        super(props)
    }

    _getPlaylist(){
        if (!this.props.trackIsLoaded) return null

        if (this.props.track)
            return this.props.track.playlist

        if (this.props.playlist)
            return this.props.playlist

        return null
    }

    _getPlaylistLink(){
        const playlist = this._getPlaylist()
        if (!playlist || !this.props.showPlaylistLink) return null

        const playlistLink = '/playlist/' + playlist.url
        return (
            <Link to={playlistLink} className="btn btn-inline btn-sm">Show</Link>
        ) 
    }

    _getTitle(){
        const playlist = this._getPlaylist()
        if (playlist)
            return this._displayTitle(playlist.name)

        return this._displayTitle("")
    }

    _displayTitle(title){
        return (
            <h4>
                Playlist <span>{title}</span>
            </h4>
        )
    }

    _getPlayer(){
        if (!this.props.trackIsLoaded) return null

        if (this.props.track)
            return (
                <iframe src={this._getTrackUrl(this.props.track.spotifyId)} width="100%" height="80" frameBorder="0" allowTransparency="true"></iframe>
            )

        if (this.props.playlist)
            return (
                <iframe src={this._getPlaylistUrl(this.props.playlist.spotifyId)} width="100%" height="80" frameBorder="0" allowTransparency="true"></iframe>
            )

        return (
            <div className="alert alert-danger">Error loading player!</div>
        )
    }

    _getTrackUrl(spotifyId){
        return 'http://embed.spotify.com/?uri=spotify:track:' + spotifyId;
    }

    _getPlaylistUrl(spotifyId){
        return 'http://embed.spotify.com/?uri=spotify:user:petegaulton:playlist:' + spotifyId;
    }

    _getPlayerClass(){
        let playerClass = "track-player-wrap"
        if (this.props.trackIsLoaded) playerClass += " track-player-loaded"
        return playerClass
    }

    _closePlayer(){
        this.props.closePlayer()
    }


    render(){
        return (
            <div className={this._getPlayerClass()}>
                <div className="track-player-close" onClick={this._closePlayer.bind(this)}>
                    <span className="fa fa-close"></span>
                </div>
                {this._getTitle()}
                {this._getPlaylistLink()}
                {this._getPlayer()}
            </div>
        )
    }
}