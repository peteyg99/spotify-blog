import React from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import store from '../store'
import routes from '../routes'
import { browserHistory } from 'react-router'
import ReactGA from 'react-ga'

export default class App extends React.Component {

  componentDidMount() {
    ReactGA.initialize('UA-86876611-2')
  }  

  _onUpdate() {    
    window.scrollTo(0, 0)
    ReactGA.pageview(window.location.pathname)
  }

  render(){
    return (
      <Provider store={store}>
        <Router history={browserHistory} routes={routes} onUpdate={this._onUpdate} />
      </Provider>
    )
  }
}
