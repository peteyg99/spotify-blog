import React from 'react'
import { Link } from 'react-router'
import PlaylistListPager from '../containers/PlaylistListPager'

export default class PlaylistList extends React.Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.willMount()
    }

    componentDidUpdate() {
        this.props.didUpdate()
    }
    
    _getPlaylistLink(playlist){
        return '/playlist/' + playlist.url
    }

    _displayPlaylistsLoading(){
        if (this.props.playlists.length > 0)
            return null

        if (this.props.isLoading)
            return (
                <div className="alert alert-info">Loading playlists...</div>
            )

        return (
            <div className="alert alert-error">No playlists</div>
        )
    }

    _displayPlaylists(){
        if (!this.props.playlists || this.props.playlists.length == 0)
            return null

        return (
            <ul className="track-list">
                    {this.props.playlists.map((playlist) => (
                        <li key={playlist.id} className="track-clickable">
                            <Link to={this._getPlaylistLink(playlist)}>
                                <span className="track-image">
                                    <img src={playlist.image} />
                                </span>
                                <span className="track-name">{playlist.name}</span>
                                <span className="track-artist">{playlist.trackCount} tracks</span>
                            </Link>
                        </li>
                    ))}
                </ul>
        )
    }

    render(){
        return (
            <div>
                <PlaylistListPager pageNum={this.props.pageNum} />
                <h1>Playlists</h1>
                {this._displayPlaylistsLoading()}
                {this._displayPlaylists()}
            </div>
        )
    }    
}