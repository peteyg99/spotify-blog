import React from 'react'
import { Link } from 'react-router'

export default class HeaderNav extends React.Component {
    constructor(props) {
        super(props)
    }

    render(){
        return (
            <header>
                <div className="container">
                    <Link className="navbar-brand" to="/">My Spotify Blog</Link>
                </div>
            </header>
        )
    }
}