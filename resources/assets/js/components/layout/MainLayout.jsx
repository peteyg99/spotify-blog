import React from 'react'
import HeaderNav from '../../containers/HeaderNav'
import TrackPlayer from '../../containers/TrackPlayer'

export default class MainLayout extends React.Component {
    constructor(props) {
        super(props)
    }

    render(){
        return (
            <div className="container-app full-height-wrap full-height-inner">
                <HeaderNav />
                <div className="container full-height-inner">
                    <div className="main-content">  
                        {this.props.children}
                    </div>
                </div>
                <TrackPlayer />
            </div>
        )
    }
}