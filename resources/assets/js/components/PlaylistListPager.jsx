import React from 'react'
import { Link } from 'react-router'

export default class PlaylistListPager extends React.Component {
    constructor(props) {
        super(props)
    }

    _isLastPage(){
        return this.props.lastPage <= this.props.pageNum
    }

    _getPrevClass(){
        return this._getBtnClass(this.props.pageNum > 0)
    }

    _getNextClass(){
        return this._getBtnClass(!this._isLastPage())
    }

    _getBtnClass(showBool){
        let btnClass = "btn btn-alt"
        if (showBool) 
            return btnClass
        
        return btnClass + " hide"
    }

    render(){
        const currentPage = this.props.pageNum
        const pageUrl = "/page/"
        const prevPage = currentPage > 0 ? (currentPage - 1) : 0 
        let nextPage = parseInt(currentPage) + 1
        if (nextPage > this.props.lastPage) nextPage = currentPage
        
        return (
            <div className="pager-wrap">
                <Link className={this._getPrevClass()} to={pageUrl + prevPage}>
                    <span className="fa fa-chevron-left"></span>
                </Link>
                <Link className={this._getNextClass()} to={pageUrl + nextPage}>
                    <span className="fa fa-chevron-right"></span>
                </Link>
            </div>
        )
    }
}
