import React from 'react'
import PlaylistList from '../containers/PlaylistList'

export default class Home extends React.Component {
    constructor(props) {
        super(props)
    }

    _loadRandomTrack(){
        this.props.loadRandomTrack()
    }
    _loadRandomPlaylist(){
        this.props.loadRandomPlaylist()
    }

    render(){
        return (
            <div className="flex-wrap">

                <aside>
                    <button className="btn" type="button" onClick={this._loadRandomTrack.bind(this)}>
                        Random track
                    </button>                    
                    <button className="btn" type="button" onClick={this._loadRandomPlaylist.bind(this)}>
                        Random playlist
                    </button>
                </aside>

                <main>
                    <PlaylistList pageNum={this.props.pageNum} />
                </main>

            </div>
        )
    }    
}