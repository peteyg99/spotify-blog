﻿// import $ from 'jquery'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers'
import thunk from 'redux-thunk'
// import createLogger from 'redux-logger'
import { browserHistory } from 'react-router'
import { locationUpdated } from './actions'

let count = 0
if (typeof(localStorage) !== "undefined") {
    count = localStorage.getItem('count',)
} 

const blankState = {
    playlistList: {
        pageNum: 0,
        lastPage: true,
        playlists: {
            list: [],
            isLoading: false
        },
    },
    loadedPlaylist: {
        playlist: null,
        isLoading: false
    },
    trackPlayer: {
        track: null,
        playlist: null,
        isLoaded: false
    }
}

//const initialState = Object.assign({}, blankState, window.INITIAL_STATE)
const initialState = Object.assign({}, blankState)

let middleware = [thunk]

// if (process.env.NODE_ENV !== 'production') {
//     let logger = createLogger({
//         collapsed: true,
//         colors: {
//             title: () => false,
//             prevState: () => `#9E9E9E`,
//             action: () => `#03A9F4`,
//             nextState: () => `#4CAF50`,
//             error: () => `#F20404`
//         }
//     })

//     middleware = [...middleware, logger]
// }

const store = createStore(reducer, initialState, applyMiddleware(...middleware))

// browserHistory.listen(location => {store.dispatch(locationUpdated(location))})

export default store