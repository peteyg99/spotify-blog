import { getJson } from '../helpers'


export const getPlaylists = (pageNum) => {
    if (pageNum == undefined) pageNum = 0
    return (dispatch) => {
        dispatch({
            type: 'PLAYLISTS_GET',
            pageNum
        })

        getJson('/api/playlists/' + pageNum)
            .then(response => {
                //console.log('response', response)
                if (response.ok)
                    return response.json()
                //else 
                    // dispatch({ 
                    //     type: CREDIT_REQUEST_GET_ERROR
                    // })
            })
            .then((json) => {
                //console.log('json', json)
                if(!json)
                    return // DO ERROR!

                dispatch({ 
                    type: 'PLAYLISTS_RESPONSE',
                    playlists: json.playlists,
                    pageNum,
                    lastPage: json.lastPage
                })
            })
    }
}


export const getPlaylist = (playlistUrl) => {
    return (dispatch) => {
        dispatch({
            type: 'PLAYLIST_GET'
        })

        getJson('/api/playlist/' + playlistUrl)
            .then(response => {
                // console.log('response', response)
                if (response.ok)
                    return response.json()
                //else 
                    // dispatch({ 
                    //     type: CREDIT_REQUEST_GET_ERROR
                    // })
            })
            .then((json) => {
                // console.log('json', json)
                if(!json)
                    return // DO ERROR!

                dispatch({ 
                    type: 'PLAYLIST_RESPONSE',
                    playlist: json.playlist
                })
            })
    }
}


export const getRandomTrack = () => {
    return (dispatch) => {
        getJson('/api/playlist/track/random')
            .then(response => {
                // console.log('response', response)
                if (response.ok)
                    return response.json()
                //else 
                    // dispatch({ 
                    //     type: CREDIT_REQUEST_GET_ERROR
                    // })
            })
            .then((json) => {
                // console.log('json', json)
                if(!json)
                    return // DO ERROR!

                dispatch({ 
                    type: 'TRACK_UPDATE',
                    track: json.track
                })
            })
    }    
}

export const getRandomPlaylist = () => {
    return (dispatch) => {
        getJson('/api/playlist/random')
            .then(response => {
                // console.log('response', response)
                if (response.ok)
                    return response.json()
                //else 
                    // dispatch({ 
                    //     type: CREDIT_REQUEST_GET_ERROR
                    // })
            })
            .then((json) => {
                // console.log('json', json)
                if(!json)
                    return // DO ERROR!

                dispatch({ 
                    type: 'PLAYLIST_UPDATE',
                    playlist: json.playlist
                })
            })
    }    
}

export const locationUpdated = (location) => {
    return (dispatch) => {
        // if (location.action === 'POP')
        //     return

        // const urlParts = location.pathname.toLowerCase().split('/')
        // if (urlParts.indexOf('page') < 0)
        //     return

        // const page = urlParts[2]
        // return dispatch({
        //     type: 'PLAYLIST_LIST_PAGE',
        //     page
        // })
    }
}
