'use strict';

import path from 'path';
import { Server } from 'http';
import Express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { Provider } from 'react-redux'
import store from './store'
import routes from './routes';
import NotFoundPage from './components/NotFoundPage';


 
// initialize the server 
const app = new Express();
const server = new Server(app);

// configure support for ejs templates
// app.set('view engine', 'ejs');

// set view engine to php-express
const phpExpress = require('php-express')({
  binPath: 'php'
});
app.engine('php', phpExpress.engine);
app.set('view engine', 'php');

// set views folder
app.set('views', './resources/assets/views');

// routing all .php file to php-express
app.all(/.+\.php$/, phpExpress.router);

// define the folder that will be used for static assets
app.use(Express.static('./public'))

// universal routing and rendering
app.get('*', (req, res) => {
  match(
    { routes, location: req.url },
    (err, redirectLocation, renderProps) => {

      // in case of error display the error message
      if (err) {
        return res.status(500).send(err.message);
      }

      // in case of redirect propagate the redirect to the browser
      if (redirectLocation) {
        return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
      }

      // generate the React markup for the current route
      let markup;
      if (renderProps) {
        // if the current route matched we have renderProps
        markup = renderToString(
            <Provider store={store}>
                <RouterContext {...renderProps}/>
            </Provider>
        );
      } else {
        // otherwise we can render a 404 page
        markup = renderToString(<NotFoundPage/>);
        res.status(404);
      }

      // render the index template with the embedded React markup
      return res.render('index', { 
        get: {
          uri: req.url,
          markup 
        }
      });
    }
  );
});

// start the server
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'production';
server.listen(port, err => {
  if (err) {
    return console.error(err);
  }
  console.info(`Server running on http://localhost:${port} [${env}]`);
});
