import { connect } from 'react-redux'
import View from '../components/PlaylistListPager'

const mapStateToProps = (state, ownProps) => {
    return {
        lastPage: state.playlistList.lastPage
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {lastPage} = stateProps
    const {pageNum} = ownProps

    return {
        pageNum,
        lastPage
    }
}

const PlaylistListPager = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default PlaylistListPager