import { connect } from 'react-redux'
import View from '../components/TrackPlayer'

const mapStateToProps = (state, ownProps) => {
    return {
        track: state.trackPlayer.track,
        playlist: state.trackPlayer.playlist,
        trackIsLoaded: state.trackPlayer.isLoaded,
        loadedPlaylist: state.loadedPlaylist.playlist
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {track, playlist, trackIsLoaded, loadedPlaylist} = stateProps
    const {dispatch} = dispatchProps

    const showPlaylistLink = !loadedPlaylist 
                                || (track && loadedPlaylist.id != track.playlist.id)
                                || (playlist && loadedPlaylist.id != playlist.id)

    return {
        track,
        playlist,
        trackIsLoaded,
        showPlaylistLink,
        closePlayer: () => {
            dispatch({ type: 'PLAYER_CLOSE' })
        }
    }
}

const PlaylistList = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default PlaylistList