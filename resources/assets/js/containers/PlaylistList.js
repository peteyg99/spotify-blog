import { connect } from 'react-redux'
import { getPlaylists } from '../actions'
import View from '../components/PlaylistList'

const mapStateToProps = (state, ownProps) => {
    return {
        playlists: state.playlistList.playlists,
        pageNum: state.playlistList.pageNum
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {playlists, pageNum} = stateProps
    const {dispatch} = dispatchProps

    let propsPageNum = ownProps.pageNum
    if (propsPageNum == undefined) propsPageNum = 0

    return {
        playlists: playlists.list,
        isLoading: playlists.isLoading,
        pageNum: propsPageNum,
        willMount: () => {
            dispatch(getPlaylists(propsPageNum))
        },
        didUpdate: () => {
            if (pageNum != propsPageNum) 
                dispatch(getPlaylists(propsPageNum))
        }
    }
}

const PlaylistList = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default PlaylistList