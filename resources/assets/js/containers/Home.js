import { connect } from 'react-redux'
import { getRandomTrack, getRandomPlaylist } from '../actions'
import View from '../components/Home'

const mapStateToProps = (state, ownProps) => {
    return {
    }
    
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        pageNum: ownProps.params.pageNum,
        loadRandomTrack: () => {
            dispatch(getRandomTrack())
        },
        loadRandomPlaylist: () => {
            dispatch(getRandomPlaylist())
        }
    }
}

const Home = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default Home