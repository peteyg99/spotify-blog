import { connect } from 'react-redux'
import View from '../components/layout/HeaderNav'

const mapStateToProps = (state, ownProps) => {
    return {
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    return {
    }
}

const HeaderNav = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default HeaderNav