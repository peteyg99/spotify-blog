import { connect } from 'react-redux'
import { getPlaylist } from '../actions'
import View from '../components/Playlist'

const mapStateToProps = (state, ownProps) => {
    const currentTrack = state.trackPlayer.track

    return {
        playlistUrl: ownProps.params.playlistUrl,
        playlist: state.loadedPlaylist,
        currentTrackId: currentTrack ? currentTrack.spotifyId : null
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {playlistUrl, playlist, currentTrackId, showExtraButtons} = stateProps
    const {dispatch} = dispatchProps

    return {
        playlist: playlist.playlist,
        isLoading: playlist.isLoading,
        currentTrackId,
        sendTrackToPlayer: (track) => {
            dispatch({ type: 'TRACK_UPDATE', track })
        },
        sendPlaylistToPlayer: () => {
            dispatch({ type: 'PLAYLIST_UPDATE', playlist: playlist.playlist })
        },
        willMount: () => {
            dispatch(getPlaylist(playlistUrl))
        },
        didUpdate: () => {
            if (!playlist.isLoading && playlist.playlist && playlist.playlist.url != playlistUrl)
                dispatch(getPlaylist(playlistUrl))
        }
    }
}

const Playlist = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default Playlist